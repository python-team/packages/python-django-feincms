Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FeinCMS
Upstream-Contact: Matthias Kestenholz <mk@spinlock.ch>
Source: https://github.com/matthiask/feincms
Files-Excluded:
 feincms/static/feincms/*.min.js

Files: *
Copyright: 2009-2011, Feinheit GmbH and contributors
License: BSD-3-clause

Files: feincms/module/medialibrary/zip.py
       feincms/signals.py
Copyright: 2011 Martin J. Laubach. All rights reserved.
License: BSD-3-clause

Files: feincms/module/extensions/ct_tracker.py
Copyright: 2009 Martin J. Laubach. All rights reserved.
License: BSD-3-clause

Files: feincms/static/feincms/jquery.cookie.js
Copyright: 2006 Klaus Hartl (stilbuero.de)
License: MIT or GPL-2+

Files: feincms/utils/queryset_transform.py
Copyright: 2010 Simon Willison
License: BSD-3-clause

Files: feincms/locale/nb/LC_MESSAGES/django.po
Copyright: 2011 Håvard Grimelid
License: BSD-3-clause

Files: feincms/locale/ca/LC_MESSAGES/django.po
 feincms/locale/es/LC_MESSAGES/django.po
Copyright: 2009, Maarten van Gompel (proycon) <proycon@anaproy.nl>
License: BSD-3-clause

Files: tests/testapp/tests/yahoo.rss
Copyright: 2013 Yahoo! Inc. All rights reserved
License: BSD-3-clause

Files: debian/*
Copyright: 2011-2013 Janos Guljas <janos@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
     1. Redistributions of source code must retain the above copyright notice, 
        this list of conditions and the following disclaimer.
 .
     2. Redistributions in binary form must reproduce the above copyright 
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
 .
     3. Neither the name of FeinCMS nor the names of its contributors may be
        used to endorse or promote products derived from this software without
        specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: MIT
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
