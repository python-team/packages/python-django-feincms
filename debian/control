Source: python-django-feincms
Maintainer: Janos Guljas <janos@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 9),
               dh-python,
               python-all,
               python3-all,
               python-setuptools,
               python3-setuptools,
               python-django,
               python3-django,
               python-sphinx,
               python3-sphinx,
               python-pil,
               python3-pil,
               python-feedparser,
               python3-feedparser,
               python-django-mptt,
               python3-django-mptt,
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-django-feincms
Vcs-Git: https://salsa.debian.org/python-team/packages/python-django-feincms.git
Homepage: https://github.com/matthiask/feincms

Package: python-django-feincms
Architecture: all
Depends: ${misc:Depends},
         ${python:Depends},
         libjs-jquery,
         libjs-jquery-ui,
         libjs-jquery-cookie
Recommends: python-lxml,
            python-beautifulsoup
Provides: ${python:Provides}
Description: Django-based Page CMS and CMS building toolkit
 FeinCMS is an extremely stupid content management system. It knows
 nothing about content -- just enough to create an admin interface for
 your own page content types. It lets you reorder page content blocks
 using a drag-drop interface, and you can add as many content blocks
 to a region (f.e. the sidebar, the main content region or something
 else). It provides helper functions, which provide ordered lists of page
 content blocks.

Package: python3-django-feincms
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         libjs-jquery,
         libjs-jquery-ui,
         libjs-jquery-cookie
Recommends: python3-lxml,
            python3-beautifulsoup
Provides: ${python3:Provides}
Description: Django-based Page CMS and CMS building toolkit
 FeinCMS is an extremely stupid content management system. It knows
 nothing about content -- just enough to create an admin interface for
 your own page content types. It lets you reorder page content blocks
 using a drag-drop interface, and you can add as many content blocks
 to a region (f.e. the sidebar, the main content region or something
 else). It provides helper functions, which provide ordered lists of page
 content blocks.

Package: python-django-feincms-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         libjs-jquery,
         libjs-underscore
Description: Django-based Page CMS and CMS building toolkit - documentation
 FeinCMS is an extremely stupid content management system. It knows
 nothing about content -- just enough to create an admin interface for
 your own page content types. It lets you reorder page content blocks
 using a drag-drop interface, and you can add as many content blocks
 to a region (f.e. the sidebar, the main content region or something
 else). It provides helper functions, which provide ordered lists of page
 content blocks.
 .
 This package contains the HTML documentation and examples.
